# Pandas Through Covid

A tour of pandas operations analyzing the daily totals of NM covid results. 

## Getting started

I download the daily csv from the network tab in the dashboard: 
https://cvprovider.nmhealth.org/public-dashboard.html

By clicking "Show Historical Statewide Data"

and save it to NM_HISTORY.json

## CovidActNow Data

Some scripts will access data from [[covidactnow.org]].  I do not include the data or keys here;  they are all freely available from [[apidocs.covidactnow.org]]. 


